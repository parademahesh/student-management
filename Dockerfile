FROM openjdk:8
EXPOSE 8000
ADD target/student-management-system.jar student-management-system.jar
ENTRYPOINT ["java","-jar","/student-management-system.jar"]
